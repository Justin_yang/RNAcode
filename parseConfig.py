import sys
import os
import parseGeneAnnotation
from configobj import ConfigObj

def parseConfig(config_file):
	"""
	Parse a configuration file.

	Args:
		config_file: string, Configuration file name.

	Returns:
		A dictionary containing parsed parameters.
	"""
	params = {}
	config = ConfigObj(config_file)

	# read input options
	params['reference'] = config['INPUT_FILES']['reference']
	params['gene_annotation_file'] = config['INPUT_FILES']['gene_annotation_file']
	params['read_plus_mate1'] = config['INPUT_FILES']['read_plus_mate1']
	params['read_plus_mate2'] = config['INPUT_FILES']['read_plus_mate2']
	params['read_minus_mate1'] = config['INPUT_FILES']['read_minus_mate1']
	params['read_minus_mate2'] = config['INPUT_FILES']['read_minus_mate2']
	params['output_path'] = config['INPUT_FILES']['output_path']

	# read global settings
	params['nthreads'] = config['GLOBAL_SETTING']['num_thread']

	# read alignment options
	params['paired_end'] = config.get('ALIGNMENT').as_bool('paired_end')
	params['bowtie_version'] = config['ALIGNMENT']['bowtie_version'].strip()
	params['bowtie_index_prefix'] = config['ALIGNMENT']['bowtie_index_prefix']
	params['bowtie_index_option'] = config['ALIGNMENT']['bowtie_index_option']
	params['bowtie_alignment_option'] = config['ALIGNMENT']['bowtie_alignment_option']

	# read reactivity calculation options
	params['output_prefix'] = config['REACTIVITY_CALC']['output_prefix']
	params['reactivity_model'] = config['REACTIVITY_CALC']['method']
	params['normalization'] = config['NORMALIZATION']['method']

	return params


def checkConfig(params):
	"""
	Check if the given parameters are valid. If invalid, output error message and then exit execution.

	Args:
		params: A dictionary containing parsed parameters.
	"""
	params['nthreads'] = params['nthreads'].strip()
	if params['nthreads'] == "" or (not params['nthreads'].isdigit()):
		params['nthreads'] = "1"	
	if not os.path.isfile(params['reference']):
		print 'Error: reference sequence file %s is missing or does not exist. Please double check the configuration file.' % params['reference']
		sys.exit(1)
	if params['gene_annotation_file'] != "" and not os.path.isfile(params['gene_annotation_file']):
		print 'Error: gene annotation file does not exist.'
		sys.exit(1)	
	if not os.path.isfile(params['read_plus_mate1']):
		print 'Error: plus chanel read file %s is missing or does not exist. Please double check the configuration file.' % params['read_plus_mate1']
		sys.exit(1)
	if 	not os.path.isfile(params['read_minus_mate1']):
		print 'Error: minus chanel read file %s is missing or does not exist. Please double check the configuration file.' % params['read_minus_mate1']
		sys.exit(1)
	if params['paired_end']:
		if not os.path.isfile(params['read_plus_mate2']):
			print 'Error: the 2nd file of plus chanel read file %s is missing or does not exist, while paired-end mode is enabled. Please double check the configuration file.' % params['read_plus_mate2']
			sys.exit(1)
		if 	not os.path.isfile(params['read_minus_mate2']):
			print 'Error: the 2nd file of minus chanel read file %s is missing or does not exist, while paired-end mode is enabled. Please double check the configuration file.' % params['read_minus_mate2']
			sys.exit(1)	
	if 	params['bowtie_version'] != 'bowtie' and params['bowtie_version'] != 'bowtie2':
		print 'Error: wrong bowtie version specified. %s is given while bowtie or bowtie2 is required.' % params['bowtie_version']
		sys.exit(1)
	if params['bowtie_index_prefix'].strip() == '':
		print 'Error: please specify bowtie_index_prefix in the configuration file.'
		sys.exit(1)
	if not params['reactivity_model'] in ['mle', 'ding']:
		print 'Error: please specify the correct method for calculating reactivities. Available options: mle or ding.'
		sys.exit(1)
	if not params['normalization'] in ['fp', 'boxplot']:
		print 'Error: please specify the correct method for normalization reactivities. Available options: fp or boxplot.'	

	if not os.path.exists(params['output_path']):
		os.makedirs(params['output_path'])
	if not os.path.exists(params['output_path'] + '/bowtie_index/'):
		os.makedirs(params['output_path'] + '/bowtie_index/')
	if not os.path.exists(params['output_path'] + '/aligned_reads/'):
		os.makedirs(params['output_path'] + '/aligned_reads/')
	if not os.path.exists(params['output_path'] + '/raw_reactivity/'):
		os.makedirs(params['output_path'] + '/raw_reactivity/')		
	if not os.path.exists(params['output_path'] + '/final_reactivity/'):
		os.makedirs(params['output_path'] + '/final_reactivity/')

def extractTranscripts(params):
	if params['gene_annotation_file'] == "":
		return
	transcript_file = params['output_path'] + '/bowtie_index/'+ params['bowtie_index_prefix'] + "_transcripts.fa"
	parseGeneAnnotation.extractTranscripts(params['reference'], params['gene_annotation_file'], transcript_file)
	params['reference'] = transcript_file				


def generateBowtieIndexCommand(params):
	"""
	Generate shell command to build bowtie index from given parameters.
	
	Args:
		params: A dictionary containing parsed parameters.

	Returns:
		A string representing desired command.
	"""
	bowtie_index_path = params['output_path'] + '/bowtie_index/' + params['bowtie_index_prefix']
	build_index_command = params['bowtie_version'] + '-build ' + params['bowtie_index_option']
	#if params['bowtie_version'] == "bowtie2":
	#	build_index_command += " --threads " + params['nthreads']
	build_index_command += 	' ' + params['reference'] + ' ' + bowtie_index_path
	return build_index_command


def generateAlignmentCommand(params):
	"""
	Generate command to build bowtie alignment from given parameters.
	
	Args:
		params: A dictionary containing parsed parameters.
	
	Returns:
		algn_command_plus: A string representing desired command for plus chanel reads alignment.
		algn_command_minus: A string representing desired command for minus chanel reads alignment.
	"""
	bowtie_index_path = params['output_path'] + '/bowtie_index/' + params['bowtie_index_prefix']
	plus_algn_output = params['output_path'] + '/aligned_reads/plus_alignment' 
	minus_algn_output = params['output_path'] + '/aligned_reads/minus_alignment'

	algn_command_plus = params['bowtie_version'] + ' ' + params['bowtie_alignment_option'] + ' '
	algn_command_minus = ''
	# Add defaulut options when not specified
	if (not "-p" in params['bowtie_alignment_option']) and (not "--threads" in params['bowtie_alignment_option']):
		algn_command_plus = algn_command_plus + ' --threads ' + params['nthreads'] + ' '
	#if not "--norc" in 	params['bowtie_alignment_option']:
	#	algn_command_plus += " --norc "
	#if not "-I" in params['bowtie_alignment_option']:
	#	algn_command_plus += " -I 1 "
	#if not "-X" in params['bowtie_alignment_option']:
	#	algn_command_plus += " -X 1000 "		
	if params['bowtie_version'] == 'bowtie2':
		#if not "--no-mixed" in params['bowtie_alignment_option']:
		#	algn_command_plus += " --no-mixed "
		#if not "--no-discordant" in params['bowtie_alignment_option']:
		#	algn_command_plus += " --no-discordant "	 	
		algn_command_plus += ' -x '
	else:
		if not "--chunkmbs" in 	params['bowtie_alignment_option']:
			algn_command_plus += " --chunkmbs 1024 "
		if not "--best" in 	params['bowtie_alignment_option']:
			algn_command_plus += " --best "
	algn_command_plus += bowtie_index_path + ' '
	algn_command_minus = algn_command_plus

	if params['paired_end']:
		algn_command_plus += ' -1 ' + params['read_plus_mate1'] + ' -2 ' + params['read_plus_mate2'] + ' '
		algn_command_minus += ' -1 ' + params['read_minus_mate1'] + ' -2 ' + params['read_minus_mate2'] + ' '
	else:
		if params['bowtie_version'] == 'bowtie2':
			algn_command_plus += ' -U '
			algn_command_minus += ' -U '
		algn_command_plus += params['read_plus_mate1'] + ' '
		algn_command_minus += params['read_minus_mate1'] + ' '
	
	algn_command_plus += ' -S '
	algn_command_minus += ' -S '
	algn_command_plus += plus_algn_output + '.sam'
	algn_command_minus += minus_algn_output + '.sam'	
	return algn_command_plus, algn_command_minus


def test(config_file):
	"""Thi is just a test function."""
	params = parseConfig(config_file)
	checkConfig(params)
	bowtie_index_command = generateBowtieIndexCommand(params)
	algn_command_plus, algn_command_minus = generateAlignmentCommand(params)
	print '\n-------------------------------'
	print 'index command:', bowtie_index_command
	os.system('%s' % bowtie_index_command)
	print

	print '\n-------------------------------'
	print 'algn_plus:', algn_command_plus
	os.system('%s' % algn_command_plus)
	print

	print '\n-------------------------------'
	print 'algn_minus:', algn_command_minus
	os.system('%s' % algn_command_minus)


























	
