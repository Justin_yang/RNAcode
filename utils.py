import os
import pysam
import HTSeq


def getReference(input_file):
    """
    Get the reference sequences and lengths from given .bam or .sam file. Note that
    the input SAM/BAM file should have the header section with the @SQ lines representing
    reference sequences.
    
    Args:
      	input_file: input alignment file in SAM or BAM format.

    Returns:
      	dict, Dictionary for reference sequences, mapping from reference_name to the length of reference sequence.
    """
    samfile = pysam.AlignmentFile(input_file)
    ref = samfile.references
    length = samfile.lengths
    dicts = {}
    for index in range(0, len(ref)):
        dicts[ref[index]] = length[index]
        samfile.close()
    return dicts


def getFileName(input_file):
    """
    Get the name of file, excluding the suffix like .txt, .sam.

    Args:
      	input_file: string, A file name.

    Returns:
      	base: string, Prefix of the file.
      	ext: string, Extension of file, starting with '.'.
    """
    base, ext = os.path.splitext(input_file)
    return base, ext


def openFile(input_file):
    """
    Open a given SAM/BAM file.
    
    Args:
      	input_file: string, File name of an alignment file in SAM or BAM format.
    
    Returns:
      	A file handler pointing to the input_file.
    """
    almnt_file = None
    if (input_file.lower().endswith('sam')):
        almnt_file = HTSeq.SAM_Reader(input_file)
    elif (input_file.lower().endswith('bam')):
        almnt_file = HTSeq.BAM_Reader(input_file)
    else:
        print "Error reading alignment file. Expected .bam or .sam format."
        sys.exit(1)
    return almnt_file


def sortAlignmentFileByQueryName(input_file):
    """
    Sort given SAM/BAM file by queryname. 

    Args:
	  	input_file: string, File name of an alignment file in SAM or BAM format.
    
    Returns:
      	string, input_file name if it is already sorted; otherwise, return the sorted file name (a 
      	temporary file name specified by the code).
    """
    samfile = pysam.AlignmentFile(input_file)
    sorted_tag = samfile.header["HD"]["SO"]
    samfile.close()
    if sorted_tag != "queryname":
        print 'sorting %s\n' % input_file
        file_base, file_ext = getFileName(input_file)
        tmp_file = file_base + '_sorted' + file_ext
        os.system('samtools sort -n -o %s %s' % (input_file, tmp_file))
        input_file = tmp_file
    return input_file


def writeReactivities(reac, outfile):
    """
    Write reactivities into file.

    Args:
        reac: dict, Dictionary containing a list of raw_reactivities for each reference sequence.
        outfile: string, File to write final reactivities.
    """
    fp = open(outfile, "w")
    for chrome in reac:
        fp.write("%s\n" % chrome)
        size = len(reac[chrome])
        for i in range(size):
            fp.write("%.3f\t" % reac[chrome][i])
        fp.write("-999")    
        fp.write('\n\n')    
    fp.close()    