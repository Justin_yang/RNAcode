import sys
import os
import parseConfig
import countReads
import calcReactivity
import normalizeReactivity

def buildBowtieIndex(params):
	"""
	Build bowtie index. If a gene annotation file is given, extract the transcripts first.
	"""
	print '\n================Building bowtie index================'
	parseConfig.extractTranscripts(params)
	bowtie_index_command = parseConfig.generateBowtieIndexCommand(params)
	print 'Using command:', bowtie_index_command
	os.system('%s' % bowtie_index_command)
	print 'Bowtie index built sucessfully!'


def doAlignment(params):
	"""
	Align plus and minus chanel.
	"""
	algn_command_plus, algn_command_minus = parseConfig.generateAlignmentCommand(params)

	print '\n================Aligning plus chanel reads================'
	print 'Using command:', algn_command_plus
	os.system('%s' % algn_command_plus)
	algn_plus_sam = params['output_path'] + '/aligned_reads/plus_alignment.sam'
	algn_plus_bam = params['output_path'] + '/aligned_reads/plus_alignment.bam'
	if params['paired_end']:
		os.system('samtools view -uSh -@ %s %s | samtools sort -n -@ %s -o %s' % (params['nthreads'], algn_plus_sam, params['nthreads'], algn_plus_bam))
	else:
		os.system('samtools view -uSh -@ %s -o %s %s' % (params['nthreads'], algn_plus_bam, algn_plus_sam))	
	#os.system('rm %s' % algn_plus_sam)
	print 'Plus chanel reads aligned sucessfully!'

	print '\n================Aligning minus chanel reads================'
	print 'Using command:', algn_command_minus
	os.system('%s' % algn_command_minus)
	algn_minus_sam = params['output_path'] + '/aligned_reads/minus_alignment.sam'
	algn_minus_bam = params['output_path'] + '/aligned_reads/minus_alignment.bam'
	if params['paired_end']:
		os.system('samtools view -uSh -@ %s %s | samtools sort -n -@ %s -o %s' % (params['nthreads'], algn_minus_sam, params['nthreads'], algn_minus_bam))
	else:
		os.system('samtools view -uSh -@ %s -o %s %s' % (params['nthreads'], algn_minus_bam, algn_minus_sam))	
	#os.system('rm %s' % algn_minus_sam)
	print 'Minus chanel reads aligned sucessfully!'
	return algn_plus_bam, algn_minus_bam


def writeAlignmentPos(params, algn_plus, algn_minus):
	"""
	Write the first and last alignment position for each meaningful read. 
	For single-end reads, "meaningful" means aligned.
	For paired-end reads, "meaningful" means both segments are aligned to the same reference sequence.
	"""
	output_file_base = params['output_path'] + '/aligned_reads/' + params['output_prefix']
	output_file_plus = output_file_base + "_plus.algn"
	output_file_minus = output_file_base + "_minus.algn"
	countReads.getAlignmentPos(algn_plus, params['paired_end'], output_file_plus)
	countReads.getAlignmentPos(algn_minus, params['paired_end'], output_file_minus)


def compRawReac(params, algn_plus_bam, algn_minus_bam):
	"""
	Compute raw reactivities.
	"""
	print '\n================Computing raw reactivities================'
	reac_output_path = params['output_path'] + '/raw_reactivity/' + params['output_prefix']
	print 'Method used:', params['reactivity_model']
	raw_reac = calcReactivity.calculateReactivity(algn_plus_bam, algn_minus_bam, params['paired_end'], reac_output_path, method=params['reactivity_model'])
	print 'Raw reactivity written to file: %s.reac' % reac_output_path
	print 'Raw reactivities computed sucessfully!'
	return raw_reac


def normReac(params, raw_reac):	
	"""
	Normalize raw reactivities.
	"""
	print '\n================Normalize raw reactivities================'
	final_reac_file = params['output_path'] + '/final_reactivity/' + params['output_prefix'] + '.reac'
	print 'Normalization method:', params['normalization']
	normalizeReactivity.normalizeReactivity(raw_reac, final_reac_file, method=params['normalization'])
	print 'Normalized reactivities written to file:', final_reac_file
	print 'Reactivities normalized sucessfully!'


def main(config_file):
	"""
	Parse configuration file and run the pipeline.
	"""
	params = parseConfig.parseConfig(config_file)
	parseConfig.checkConfig(params)

	buildBowtieIndex(params)
	algn_plus_bam, algn_minus_bam = doAlignment(params)
	writeAlignmentPos(params, algn_plus_bam, algn_minus_bam)

	raw_reac = compRawReac(params, algn_plus_bam, algn_minus_bam)
	normReac(params, raw_reac)
	print 'Data analysis pipeline completed!'


def printVersion():
	version = "0.01"
	print version


def printUsage():
	usage = """Usage: python RNAcode.py <config_file>"""
	print usage


if __name__ == "__main__":
	if len(sys.argv) != 2:
		printUsage()
		exit(1)
	config_file = sys.argv[1]
	main(config_file)	