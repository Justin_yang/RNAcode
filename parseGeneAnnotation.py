import HTSeq
import os

def extractTranscripts(sequence_file, gff_file, out_file):
    """
    Extract all transcripts sequences from a Fasta file based on a GFF/GTF file. The resulting
    transcripts are writting to disk in Fasta format.
    
    Params:
        sequence_file: a fasta file.
        gff_file: a GFF/GTF file.
        out_file: output file to write resulting transcripts.
    """
    seq = dict((s.name, s) for s in HTSeq.FastaReader(sequence_file))
    gff_end_included = checkGFFBoundary(gff_file)
    gff = HTSeq.GFF_Reader(gff_file, end_included=True)
    fout = open(out_file, "w")
    for feature in gff:
        if feature.type == 'transcript':
            fragment = getATranscriptome(seq, feature)
            fout.write('>%s\n%s\n' % (feature.name, fragment))
    fout.close()

def getATranscriptome(sequences, feature):
    """
    Retrive a single transcript based on a given entry in GFF/GTF file.
    
    Params:
        sequences: dict, contains sequence_name:sequence pair.
        feature: HTSeq feature, a single line from a GFF/GTF file.
        
    Returns:
        The given sequence fragment (or its reverse complement) if the feature is from + (or -) strand of a chromosome.
    """
    chrom, strand = feature.iv.chrom, feature.iv.strand
    start, end = feature.iv.start, feature.iv.end
    fragment = sequences[chrom][start:end]
    if strand == '-':
        return fragment.get_reverse_complement()
    else:
        return fragment

def checkGFFBoundary(gff_file):
    """
    Check if the end position of each feature is included. 
    
    Params:
        gff_file: a GFF/GTF file.
    
    Returns:
        True if the end position is included.
    """
    gff = HTSeq.GFF_Reader(gff_file)
    for feature in gff:
        if feature.type == "CDS" or feature.type == "stop_codon":
            if (feature.iv.end - feature.iv.start) % 3 == 0:
                return True
            else:
                return False
    return True

def test():
	extractTranscripts("Saccharomyces_cerevisiae.R64-1-1.dna.toplevel.fa", "Saccharomyces_cerevisiae.R64-1-1.84.gtf", "/Users/feideng/Dropbox/Research/RNAcode/test/my.fa")