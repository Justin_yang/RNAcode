import random # included for test PURPOSE
import math
import utils


def normalizeReactivity(raw_reac, final_reac_file, method="fp"):
    """
    Normalize reactivities.

    Args:
        raw_reac: dict, Dictionary containing a list of raw_reactivities for each reference sequence.
        method: string, The normalization method to be chosen. Two options are available: "mle" and "boxplot".
        outfile: string, File to write final reactivities.

    Returns:
        dict, Similar to raw_reac, but with normalized reactivities.
    """
    final_reac = {}
    for chrome in raw_reac:
        normalized_reac = None
        if method.lower() == "fp":
            normalized_reac =  fixedParameterNormalization(raw_reac[chrome], 2, 8)
        else:
            normalized_reac =  boxplotNormalization(raw_reac[chrome])
        final_reac[chrome] = normalized_reac    
    utils.writeReactivities(final_reac, final_reac_file)
    return final_reac


def fixedParameterNormalization(reac_list, percent1, percent2):
    """
    Perform fixed parameter normalization on a list of numbers.

    Args:
        reac_list: list, A list of reactivities.
        percent1 - int, The percentage of numbers to be removed.
        percent2 - int, The percentage of numbers used to calculate scaling factor.

    Returns:
        The resulting list of reactivities.

    Example:
        fixedParameterNormalization(reac, 2, 8) will perform 2-8% normalization on reac.
    """
    if percent1 < 0 or percent2 <= 0 or percent1 >= 100 or percent1 + percent2 > 100:
        print "Error: please use correct percentages for fixed parameter normalization!"
        sys.exit(0)
    # handling possible missing value represented by -999
    tmp = [x if x > -500 else 0 for x in reac_list]
    sorted_reac = sorted(tmp, reverse=True)
    n = len(reac_list)
    num1 = int(0.01 * n * percent1)
    num2 = int(0.01 * n * percent2)
    next_top = sorted_reac[num1 : num1 + num2]
    if num2 == 0:
        return reac_list
    scaling_factor = sum(next_top) / num2
    if scaling_factor == 0:
        return reac_list
    normalized_reac = [round(x / scaling_factor,3) if x > -500 else -999 for x in reac_list]
    return normalized_reac


def boxplotNormalization(data):
    """
    Perform boxplot normalization on given list of reactivities as follows: Values greater than
    1.5x the interquartile range (numerical distance between the 25th and 75th percentiles) above
    the 75th percentile are removed. After excluding these outliers, the next 10% of
    reactivities are averaged, and all reactivities (including outliers) are divided by this value.

    Args:
        data: list, A list of reactivities.

    Returns:
        The resulting list of reactivities.
    """
    scaling_factor = calcScalingFactor(data)
    result = [0.0] * len(data)
    for i in range(0, len(data)):
        if data[i] > -500:
            result[i] = round(data[i] / scaling_factor, 3)
        else:
            result[i] = -999
    return result


def calcScalingFactor(data):
    """
    Calculate scaling factor for boxplot normalization.

    Args:
        data - list, A list of reactivities.

    Returns:
        float, Calculated scaling factor.
    """
    if len(data) < 10:
        return 1.0
    # handle missing reactivities represented by -999
    all_data = [x if x > -500 else 0 for x in data]
    all_data = sorted(all_data, reverse=True)
    threshold = 1.5 * abs(percentile(all_data, 0.75) - percentile(all_data, 0.25))
    remaining_data = []
    for i in range(0, len(all_data)):
        if all_data[i] < threshold:
            remaining_data.append(all_data[i])
    tenPercent = int(len(all_data) * 0.1)
    size = min(tenPercent, len(remaining_data))
    if size == 0:
        return 1.0
    total = 0
    for i in range(0, size):
        total += remaining_data[i]
    scaling_factor = total / size
    return scaling_factor


def percentile(data, percent):
    """
    Get percentile from a sorted list of numbers.

    Args:
        data: A sorted list of reactivities in descending order.
        percent: float, A number between 0 and 1.

    Returns:
        float, The percentile of the values in the list.
    """
    n = len(data)
    frac, j = math.modf((n - 1) * percent)
    j = int(j)
    if j < 0:
        return data[0]
    if j >= n - 1:
        return data[n - 1]
    return data[j] + (data[j + 1] - data[j]) * frac

def test_func():
    """This is just a test function."""
    random.seed(0)
    test = [0.0] * 100
    for i in range(100):
        test[i] = round(random.random(), 3)
    test[5] = -999
    test[98] = -999

    normdata = fixedParameterNormalization(test, 2, 8)

test_func()
